export const texts = [
  'ahahaha',
  'Clara ukrala u karla korali',
  'A segodnya v zavtrashniy den ne vse mogut smotret',
  'Making a mother of all omelets here, Jack. Cant fret for every egg.',
  'Foolishness Dante, power controls everything. Without it you cant protect anyone, let alone yourself',
  'Ive rejected my humanity JoJo',
  'My name is Yoshikage Kira. Im 33 years old. My house is in the northeast section of Morioh, where all the villas are, and I am not married. I work as an employee for the Kame Yu department stores, and I get home every day by 8 PM at the latest. I dont smoke, but I occasionally drink. Im in bed by 11 PM, and make sure I get eight hours of sleep, no matter what. After having a glass of warm milk and doing about twenty minutes of stretches before going to bed, I usually have no problems sleeping until morning. Just like a baby, I wake up without any fatigue or stress in the morning. I was told there were no issues at my last check-up. Im trying to explain that Im a person who wishes to live a very quiet life. I take care not to trouble myself with any enemies, like winning and losing, that would cause me to lose sleep at night. That is how I deal with society, and I know that is what brings me happiness. Although, if I were to fight I wouldnt lose to anyone.',
];

export const comments = {
  start: (name, transport) => {
    return `Let's meet ${name} who will be using ${transport.name} which ${transport.description}`;
  },
  near: (name) => `${name} is finally nearing the finishing line.`,
  status: (name, place, progress) => {
    return `${name} is on place ${place} with ${progress}% completion. `;
  },
  finish: (name, place) => {
    return `${name} finished while being at place ${place}. `;
  },
  end: (name, place) => `${place}.${name} `,
  jokes: [
    'Knock knock. Whos there? Tishina. Otkrivayu dver a tam armyane v nardi igrayut.',
    'They call me agent 007, 0 fixes, 0 features, 7 compilation errors.',
    '"This regex is perfectly understandable" is something Ive never said.',
    'Smh instead of commenting Im looking at memes.',
    'I was an adventurer but then i got my knee shot',
  ],
};

export const transport = [
  {
    name: 'Logitech K120',
    description:
      'is a cheap and durable keybord but not really impressive considering its lack of lgbt lighting.',
  },
  {
    name: 'Razer Huntsman Elite Ultra Pro Black Edition',
    description:
      'shows us that our contestant is a real, true, some might even say genuine GAMER.',
  },
  {
    name: 'IBM Keyboard Model M',
    description:
      'is like I dont know man, that keyboard is probably older than me.',
  },
  {
    name: 'Gembird SM-TH-1-DNT-KNW',
    description:
      'a cheap piece of plastic made using child labor in Pakistan, but hey at least it has rgbq lighting.',
  },
  {
    name: 'Custom made keyboard',
    description:
      'has custom almost silent switches, wooden finish and best overall ergonomics. Did you really think that you are impressing anyone with your wastefull spendings?',
  },
];

export default { texts };
