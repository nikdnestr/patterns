export default class Users {
  constructor() {
    this.list = [];
  }

  getUser = (name) => {
    // for (let i = 0; i < this.list.length; i++) {
    //   const user = this.list[i];
    //   if (user.name === name) {
    //     return user;
    //   }
    // }
    // return null;
    return this.#findUser(name);
  };

  getUsers = () => {
    return this.list;
  };

  addUser = (user) => {
    this.list.push(user);
  };

  removeUser = (name) => {
    for (let i = 0; i < this.list.length; i++) {
      const user = this.list[i];
      if (user.name === name) {
        this.list.splice(i, 1);
      }
    }
  };

  updateUserProgress = (name, progress) => {
    // for (let i = 0; i < this.list.length; i++) {
    //   const user = this.list[i];
    //   if (user.name === name) {
    //     user.progress = progress;
    //   }
    // }
    this.#findUser(name).progress = progress;
  };

  getUserProgress = (name) => {
    // for (let i = 0; i < this.list.length; i++) {
    //   const user = this.list[i];
    //   if (user.name === name) {
    //     return user.progress;
    //   }
    // }
    // return null;
    return this.#findUser(name).progress;
  };

  #findUser = (name) => {
    for (let i = 0; i < this.list.length; i++) {
      if (this.list[i].name === name) {
        return this.list[i];
      }
    }
    return null;
  };

  getUserStatus = (name) => {
    return this.#findUser(name).status;
  };

  updateUserStatus = (name, status) => {
    this.#findUser(name).status = status;
  };

  getAllStatus = () => {
    for (let i = 0; i < this.list.length; i++) {
      if (this.list[i].status === false) {
        return false;
      }
      return true;
    }
  };

  getBestProgress = () => {
    let best = { name: 'nobody', progress: 1 };
    for (let i = 0; i < this.list.length; i++) {
      const user = this.list[i];
      if (best.progress < user.status) {
        best = user;
      }
    }
    return best;
  };
}
