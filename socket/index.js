import * as config from './config';
import { texts, comments, transport } from '../data';
import User from './User';
import Commentator from './Commentator';

export default (io) => {
  const users = new User();
  const commentator = new Commentator(comments, transport);
  let places = 0;
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username;
    users.getUser(username)
      ? socket.emit('USER_EXISTS')
      : users.addUser({ name: username, status: false, progress: 0 });
    io.emit('NEW_USER', users.getUsers());
    socket.on('disconnect', () => {
      users.removeUser(username);
    });
    socket.on('MADE_PROGRESS', (user) => {
      users.updateUserProgress(user.name, user.progress);
      io.emit('MADE_PROGRESS', user);
      if (users.getUserProgress(user.name) === 100) {
        places++;
        io.emit('FINISH', commentator.finish(user.name, places));
      }
    });
    socket.on('USER_READY', (name) => {
      users.updateUserStatus(name, true);
      if (users.getAllStatus() === true) {
        io.emit('GAME_START', {
          time: config.SECONDS_TIMER_BEFORE_START_GAME,
          comment: commentator.gameStart(users.getUsers()),
        });
      }
      io.emit('USER_READY', users.getUser(name));
    });
    socket.on('USER_UNREADY', (name) => {
      users.updateUserStatus(name, false);
      io.emit('USER_UNREADY', users.getUser(name));
    });
    socket.on('TIMER_END', () => {
      socket.local.emit('TIMER_END', {
        data: texts,
        time: config.SECONDS_FOR_GAME,
      });
    });
    socket.on('GAME_END', () => {
      socket.local.emit('GAME_END', commentator.gameEnd(users.getUsers()));
    });
    socket.on('THIRTY_SECONDS_PASSED', (time) => {
      socket.emit(
        'THIRTY_SECONDS_PASSED',
        commentator.gameStatus(users.getUsers(), time)
      );
    });
    socket.on('JOKE_TIME', () => {
      socket.emit('JOKE_TIME', commentator.tellJoke());
    });
    socket.on('NEAR_FINISH', (name) => {
      socket.emit('NEAR_FINISH', commentator.nearingFinish(name));
    });
    // socket.local.emit('rooms update', updateRooms(rooms));
    // socket.on('room create', (name) => {
    //   const room = { name: name, users: [username] };
    //   rooms.push(room);
    //   io.emit('room create', room);
    // });
    // socket.on('room entered', (name) => {
    //   rooms = addUser(name, username);
    //   socket.join(name);
    //   socket.local.emit('room entered', getRoom(name));
    // });
  });
};

// const updateRooms = (rooms) => {
//   const appropriateRooms = [];
//   for (let i = 0; i < rooms.length; i++) {
//     if (rooms[i].users < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
//       appropriateRooms.push(rooms[i]);
//     }
//   }
//   return appropriateRooms;
// };

// const addUser = (room, user) => {
//   return rooms.map((item) => {
//     if (item.name === room) {
//       let newRoom = item;
//       newRoom.users.push(user);
//       return newRoom;
//     }
//     return { ...item };
//   });
// };

// const getRoom = (name) => {
//   for (let i = 0; i < rooms.length; i++) {
//     if (rooms[i].name === name) {
//       return rooms[i];
//     }
//     return null;
//   }
// };
