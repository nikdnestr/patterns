export default class Commentator {
  constructor(comments, transport) {
    this.comments = comments;
    this.transport = transport;
  }

  greet = () => {
    return this.comments.greetings[0];
  };

  gameStart = (names) => {
    // let string = '';
    // const random = Math.floor(Math.random() * this.transport.length);
    // for (let i = 0; i < names.length; i++) {
    //   string += `${this.comments.start(
    //     names[i].name,
    //     this.transport[random]
    //   )} `;
    // }
    // return string;
    return this.#parseList(names, 'start');
  };

  gameStatus = (names, time) => {
    let string = `${time} seconds left until the end of match. `;
    return string + this.#parseList(names, 'status');
  };

  gameEnd = (names) => {
    let string = 'So lets see. ';
    return string + this.#parseList(names, 'end');
  };

  finish = (name, place) => {
    return this.#makeComment('finish', name, place);
  };

  nearingFinish = (name) => {
    return this.#makeComment('near', name);
  };

  tellJoke = () => {
    return this.#makeComment('joke');
  };

  #parseList = (list, query) => {
    let string = '';
    const sorted = list.sort((a, b) => b.progress - a.progress);
    for (let i = 0; i < sorted.length; i++) {
      const item = sorted[i];
      string += this.#makeComment(query, item.name, i + 1, item.progress);
    }
    return string;
  };

  #makeComment = (query, ...info) => {
    let random;
    switch (query) {
      case 'start':
        return this.comments.start(info[0], this.transport[info[1]]);
      case 'status':
        return this.comments.status(info[0], info[1], info[2]);
      case 'near':
        return this.comments.near(info[0]);
      case 'finish':
        return this.comments.finish(info[0], info[1]);
      case 'end':
        return this.comments.end(info[0], info[1]);
      case 'joke':
        random = Math.floor(Math.random() * this.comments.jokes.length);
        return `Time for unfunny joke. ` + this.comments.jokes[random];
      default:
        return 'I wanted to say something but I forgot what exactly.';
    }
  };
}
