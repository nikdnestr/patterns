const username = sessionStorage.getItem('username');
if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username: username } });

const userList = document.getElementById('user-list');
const gameArea = document.getElementById('game-area');
const textArea = document.getElementById('text-area');
let quitButton = document.getElementById('quit-btn');
const comments = document.getElementById('comments');

const makeReadyButton = (button) => {
  button.addEventListener('click', () => {
    socket.emit('USER_READY', username);
    button.textContent = 'unready';
    makeUnreadyButton(button);
  });
  return button;
};

const makeUnreadyButton = (button) => {
  button.addEventListener('click', () => {
    socket.emit('USER_UNREADY', username);
    readyButton.textContent = 'ready';
    makeReadyButton(button);
  });
};

const makeComment = (text) => {
  const li = document.createElement('li');
  li.textContent = text;
  li.classList.add('comment');
  return li;
};

const initReadyButton = () => {
  const readyButton = document.createElement('button');
  readyButton.classList.add('btn');
  readyButton.id = 'ready-btn';
  readyButton.textContent = 'ready';
  makeReadyButton(readyButton);
  return readyButton;
};

gameArea.appendChild(initReadyButton());

const initQuitButton = (button) => {
  const quitButton = button.addEventListener('click', () => {
    socket.emit('disconnect');
    window.location.replace('/login');
    sessionStorage.removeItem('username');
  });
  return quitButton;
};

quitButton = initQuitButton(quitButton);

const makeUser = (name) => {
  const container = document.createElement('li');
  const html = `
      <h3><div id="${name}-ready" class="ready ready-status-red"></div>${name}</h3>
        <div class="bar">
        <div id="${name}s-progress" class="progress" style="width: 0px"></div>
      </div>
    `;
  container.innerHTML = html;
  return container;
};

socket.on('NEW_USER', (users) => {
  userList.textContent = '';
  for (let i = 0; i < users.length; i++) {
    userList.appendChild(makeUser(users[i].name));
  }
});

socket.on('MADE_PROGRESS', (user) => {
  let bar = document.getElementById(`${user.name}s-progress`);
  if (user.progress === 100) {
    bar.classList.add('ready-status-green');
    console.log(bar);
  }
  bar.style.width = `${user.progress}%`;
});

socket.on('FINISH', (comment) => {
  comments.appendChild(makeComment(comment));
});

socket.on('USER_READY', (user) => {
  const ready = document.getElementById(`${user.name}-ready`);
  ready.classList.replace('ready-status-red', 'ready-status-green');
});

socket.on('USER_UNREADY', (user) => {
  const ready = document.getElementById(`${user.name}-ready`);
  ready.classList.replace('ready-status-green', 'ready-status-red');
});

socket.on('USER_EXISTS', () => {
  window.location.replace('/login');
  sessionStorage.removeItem('username');
  alert('Username with such name already exists');
});

socket.on('GAME_START', ({ time, comment }) => {
  const readyButton = document.getElementById('ready-btn');
  readyButton.classList.add('display-none');
  const timerElement = document.createElement('div');
  timerElement.classList.add('center');
  timerElement.id = 'timer';
  gameArea.appendChild(timerElement);
  comments.appendChild(makeComment(comment));
  let timer = setInterval(function () {
    if (time <= 0) {
      clearInterval(timer);
      socket.emit('TIMER_END');
      document.getElementById('timer').classList.add('display-none');
    } else {
      document.getElementById('timer').innerHTML = time;
    }
    time -= 1;
  }, 1000);
});

socket.on('TIMER_END', ({ data, time }) => {
  const text = document.createElement('div');
  text.classList.add('center');
  const rand = data[Math.floor(Math.random() * data.length)];
  const words = rand.split('');
  for (let i = 0; i < words.length; i++) {
    let word = words[i];
    let element = document.createElement('span');
    element.id = `word${i}`;
    element.textContent = word;
    text.appendChild(element);
  }
  textArea.classList.remove('display-none');
  textArea.appendChild(text);
  let num = 0;
  let closeCheck = true;
  document.addEventListener('keydown', function (e) {
    if (num < rand.length) {
      if (e.key === rand[num]) {
        let progress = ((num + 1) / rand.length) * 100;
        document.getElementById(`word${num}`).classList.add('bg-green');
        num++;
        let prog = { name: username, progress: progress };
        socket.emit('MADE_PROGRESS', prog);
        if (progress >= 70 && closeCheck) {
          socket.emit('NEAR_FINISH', username);
          closeCheck = false;
        }
      } else {
        document.getElementById(`word${num}`).classList.add('bg-red');
      }
    }
  });
  const gameTimer = document.createElement('span');
  gameTimer.id = 'game-timer';
  gameArea.appendChild(gameTimer);
  const jokeTime = Math.floor(Math.random() * time);
  let timer = setInterval(function () {
    if (time <= 0) {
      clearInterval(timer);
      socket.emit('GAME_END');
      document.getElementById('game-timer').classList.add('display-none');
    } else {
      if (time % 30 === 0) {
        socket.emit('THIRTY_SECONDS_PASSED', time);
      }
      if (time === jokeTime) {
        socket.emit('JOKE_TIME');
      }
      document.getElementById('game-timer').innerHTML =
        time + ' seconds remaining';
    }
    time -= 1;
  }, 1000);
});

socket.on('THIRTY_SECONDS_PASSED', (comment) => {
  comments.appendChild(makeComment(comment));
});

socket.on('JOKE_TIME', (comment) => {
  comments.appendChild(makeComment(comment));
  comments.appendChild(
    makeComment('You were supposed to laugh. Well whatever.')
  );
});

socket.on('GAME_END', (comment) => {
  comments.appendChild(makeComment(comment));
  comments.appendChild(
    makeComment(
      'The game is finished. Now come on dont make others wait. Get out of the room and let the other contestants enter'
    )
  );
});

socket.on('NEAR_FINISH', (comment) => {
  comments.appendChild(makeComment(comment));
});
